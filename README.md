# yaqd-nidaq

[![PyPI](https://img.shields.io/pypi/v/yaqd-nidaq)](https://pypi.org/project/yaqd-nidaq)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqd-nidaq)](https://anaconda.org/conda-forge/yaqd-nidaq)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqd-nidaq/-/blob/master/CHANGELOG.md)

Daemon for controlling input and output of national instruments DAQ

This package contains the following daemon(s):

- https://yaq.fyi/daemons/my-daemon