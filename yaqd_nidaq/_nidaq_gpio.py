__all__ = ["NidaqGpio"]

import asyncio
from typing import Dict, Any, List

from yaqd_core import ContinuousHardware
import nidaqmx as ni
#from .__version__ import __branch__


class NidaqGpio(ContinuousHardware):
    _kind = "nidaq-gpio"
    _version = "0.1.0" #+ f"+{__branch__}" if __branch__ else ""
    traits: List[str] = []
    defaults: Dict[str, Any] = {"pin":"ao0",
                                "mode":"analog",
                                "io":"output",
                                "auto-start":True,}

    def __init__(self, name, config, config_filepath):
        super().__init__(name, config, config_filepath)
        # Perform any unique initialization
        self.task = ni.Task()
        self.pin = config['pin'] #which device and analog pin
        self.mode = config['mode'] #digital or analog inputs and outputs
        self.io = config['io']
        self.autostart = config['auto-start']
        
        if self.mode  == "digital":
            if self.io == 'input':
                self.task.di_channels.add_di_channels(f'Dev1/port{self.pin[0]}/line{self.pin[1]}')
            elif self.io == 'output':
                self.task.do_channels.add_do_channels(f'Dev1/port{self.pin[0]}/line{self.pin[1]}')
        elif self.mode == 'analog':
            if self.io == 'input':
                self.task.ai_channels.add_ai_voltage_channels(f'Dev1/{self.pin}')
            elif self.io == 'output':
                self.task.ao_channels.add_ao_voltage_channels(f'Dev1/{self.pin}')
        
    def _load_state(self, state):
        """Load an initial state from a dictionary (typically read from the state.toml file).

        Must be tolerant of missing fields, including entirely empty initial states.

        Parameters
        ----------
        state: dict
            The saved state to load.
        """
        super()._load_state(state)
        # This is an example to show the symetry between load and get
        # If no persistent state is needed, these unctions can be deleted
        self.value = state.get("value", 0)

    def get_state(self):
        state = super().get_state()
        state["value"] = self.value
        return state

    def set_position(self, position):
        if self.mode == 'analog':
            assert position <= 5
            self.task.write(position, auto_start = self.autostart)
        elif self.mode == 'digial':
            assert position == 1 or position == 0
            if position == 1:
                self.task.write([True],auto_start = self.autostart)
            elif position == 0:
                self.task.write([False],auto_start= self.autostart)
            else:
                print('digital only takes a 1 or 0 input')
    
    def get_position(self):
        return self.task.read()
    
    async def update_state(self):
        """Continually monitor and update the current daemon state."""
        # If there is no state to monitor continuously, delete this function
        while True:
            # Perform any updates to internal state
            # There must be at least one `await` in this loop
            # This one waits for something to trigger the "busy" state
            # (Setting `self._busy = True)
            # Otherwise, you can simply `await asyncio.sleep(0.01)`
            self._busy = False
            if self.mode == "output":
                await self._busy_sig.wait()
            else:
                await asynico.sleep(0.01)
if __name__ == "__main__":
    NidaqGpio.main()